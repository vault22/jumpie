# Jumpie
Jumpie is an SSH application that allows you to quickly and securely connect to a target from a list of options with just a few keyboard presses. You also have the option to enter a bastion jump host in order to proxy jump before connecting directly, ensuring an extra layer of security for your connection. With this powerful tool, you can easily access remote hosts without having to worry about authentication or encryption issues.

![Example GIF](./example.gif)

## Configuration

Jumpie has three configuration files:

- hostnames.cfg - The configuration for the list of target hostnames to SSH to.
- keys.cfg - Optional SSH key variables for public key authentication.
- bastion.cfg - Optional bastion host configuration for ProxyJump mode.

To enable key authentication, simply set the CLIENT_SSH_KEY variable in the keys.cfg file to the path of your private SSH key (i.e `/home/user/.ssh/id_rsa`).

To enable the use of a bastion host, set the BASTION_HOST variable in bastion.cfg to point to your bastion jump host, i.e `user@192.168.1.100`.

## Installing Gum Dependency

This program requires Gum to work properly. To install Gum, follow these instructions:

Use a package manager:

```
bash
# macOS or Linux
brew install gum

# Arch Linux (btw)
pacman -S gum

# Nix
nix-env -iA nixpkgs.gum
# Or, with flakes
nix run "github:charmbracelet/gum" -- --help

# Debian/Ubuntu

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg
echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
sudo apt update && sudo apt install gum


# Fedora/RHEL

echo '[charm]
name=Charm
baseurl=https://repo.charm.sh/yum/
enabled=1
gpgcheck=1
gpgkey=https://repo.charm.sh/yum/gpg.key' | sudo tee /etc/yum.repos.d/charm.repo
sudo yum install gum


# Alpine
apk add gum

# Android (via termux)
pkg install gum

# Windows (via WinGet or Scoop)
winget install charmbracelet.gum
scoop install charm-gum
```


Or download it:

[Packages](https://github.com/charmbracelet/gum/releases) are available in Debian, RPM, and Alpine formats

[Binaries](https://github.com/charmbracelet/gum/releases) are available for Linux, macOS, Windows, FreeBSD, OpenBSD, and NetBSD

Or just install it with go:

go install github.com/charmbracelet/gum@latest

## TODO
- [x] Add ability to use SSH keys when connecting
- [x] Add ability to store if using bastion host or not in config file 
- [ ] Ability to use pubkey auth when connecting from bastion host to jump target
