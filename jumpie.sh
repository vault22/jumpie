#!/bin/bash
CONF_DIR="$HOME/.config/jumpie"
CONF_FILE="$HOME/.config/jumpie/hostnames.cfg"
KEY_CONF="$HOME/.config/jumpie/keys.cfg"
BASTION_CONF="$HOME/.config/jumpie/bastion.cfg"

# Function to create the main configuration file at $HOME/.config/jumpie, returns an error if it fails
createMainConf() {
  printf "Proceeding to create hostname configuration file...\n"
  if ! ERROR=$(touch "$CONF_FILE"); then
    printf "An error occured when attempting to create the hostname configuration file for jumpie.\n"
    printf "Here is the error message:\n"
    printf %s "$ERROR"
  else
    printf "Config file has successfully been created at %s ${CONF_FILE}\n\n"
  fi
}

# Function to create the SSH key configuration file at $HOME/.config/jumpie, returns an error if it fails
createKeyConf() {
  printf "Proceeding to create key configuration file...\n"
  # Did an error occur?
  if ! ERROR=$(touch "$KEY_CONF"); then
    printf "An error occured when attempting to create the key configuration file for jumpie.\n"
    printf "Here is the error message:\n"
    printf %s "$ERROR"
  else
    printf "Key config file has successfully been created at %s ${KEY_CONF}\n\n"
  fi
}

# Function to create the bastion hostname configuration file at $HOME/.config/jumpie, returns an error if it fails
createBastionConf() {
  printf "Proceeding to create bastion configuration file...\n"
  # Did an error occur?
  if ! ERROR=$(touch "$BASTION_CONF"); then
    printf "An error occured when attempting to create the bastion configuration file for jumpie.\n"
    printf "Here is the error message:\n"
    printf %s "$ERROR"
  else
    printf "Key config file has successfully been created at %s ${BASTION_CONF}\n\n"
  fi
}

# Check if config directory exists
if [ ! -d "$CONF_DIR" ]; then
  printf "Config directory does not yet exist, attempting to create it now...\n"

  # If the mkdir command fails
  if ! ERROR=$(mkdir "$CONF_DIR"); then
    printf "An error occured when attempting to create the config directory for jumpie.\n"
    printf "Here is the error message:\n" 
    printf %s "$ERROR"
  else
    printf "Config directory has successfully been created at %s ${HOME}/.config/jumpie.\n\n"
  fi

  createMainConf
  createKeyConf
  createBastionConf

fi

# Grab ssh key variables
source $KEY_CONF
# Grab bastion hostname
source $BASTION_CONF


# Read the file into an array
mapfile -t hostnames < "$CONF_FILE"


# Check if gum is installed 
if gum -v >/dev/null 2>&1; then

  # If the bastion host variable is empty
  if [ -z "$BASTION_HOST" ]; then
    BASTION=false
  else
    BASTION=true
  fi

  # If the user has a bastion jump host
  if [ "$BASTION" == true ]; then
    # If the client ssh key variable is not set
    if [ -z "$CLIENT_SSH_KEY" ]; then
      # No need for pubkey auth
      SSH_COMMAND="ssh -J ${BASTION_HOST}"
    else
      # Pubkey auth mode
      SSH_COMMAND="ssh -i ${CLIENT_SSH_KEY} -J ${BASTION_HOST}"
    fi
  else
    # If the client SSH key variable is empty
    if [ -z "$CLIENT_SSH_KEY" ]; then
      SSH_COMMAND="ssh"
    else
      SSH_COMMAND="ssh -i ${CLIENT_SSH_KEY}"
    fi
  fi

  # Use the array in the command
  JUMP_TARGET=$(gum choose --cursor.foreground=212 --header="Select jump target..." --cursor=">>>: "  "${hostnames[@]}")
  USERNAME=$(gum input --prompt.foreground=212 --prompt "Enter username >>>: ")

  $SSH_COMMAND "$USERNAME"@"$JUMP_TARGET"

else
  printf "This program has not yet implemented dependency-free mode (where it doesn't require gum), please install gum and run the program again.\n\n"
fi
